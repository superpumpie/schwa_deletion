We split our data into 60% training data and 40% testing data. 

The best MLP model has the following hyper-parameters configuration: 2 hidden layers containing, 256 and 48 hidden neurons, respectively,  with a relu activation function; batch size of 128 samples; adam optimizer. This model is optimally trained after 16 epochs. 

The best CNN model has the following hyper-parameters configuration: 7 hidden layers containing a one-dimensional temporal convolution layer with 32 output filters, a pooling layer, a dropout layers with a dropout rate of 0.75, a flatten layer, a dense layer with 512 hidden neurons and with a relu activation function. This is followed by another dropout layer with a dropout rate of 0.75, and a dense layer with one unit and a sigmoid activation function. The model is trained to minimize cross-entropy and uses an adam optimizer. Batch size is 128 samples; adam optimizer and the model is optimally trained after 16 epochs. 

The best LSTM has the following hyper-parameters configuration: It contains five hidden layers. First is an embedding layer with 128 input dimensions and 64 output dimensions. This is followed by a layer with 128 hidden neurons, a dropout layer, another layer with 128 neurons, and a second dropout layer. The hidden layers feed into a final layer of one neuron. Batch size is 128 samples and the model is optimally trained after 10 epochs. 

The results are given in table below, best produced with latex.

Model  Acc.  F1-sc.  Epochs   Footprint
MLP  78.39%  0.157} & 16 & {\bf 206 KB} \\

\begin{table}[t]
\small
\begin{tabular}{|l|c|c|c|c|}
\hline
{\bf Model} & {\bf Acc.} & {\bf F1-sc.} & {\bf Epochs} & {\bf Footprint} \\
\hline
MLP & 78.39\% & {\bf 0.157} & 16 & {\bf 206 KB} \\
CNN & {\bf 81.89\%} & 0.143 & 16 & 634 KB \\
LSTM & 57.37\% & 0.105 & {\bf 8} & 1892 KB \\
 \hline
\end{tabular}
\caption{The accuracy rates, F1-scores, epochs, and footprints for the various models for which we split training and testing data. The results are calculated on the testing set.}
\label{table:trainingtesting}
\end{table}